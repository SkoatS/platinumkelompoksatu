import { BrowserRouter, Routes, Route } from 'react-router-dom';
import RockPage from './components/games/GameRock.js';
import HomePage from './pages/home/index';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/">
          <Route index element={<HomePage />} />
          <Route path="/rockpaper" element={<RockPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
